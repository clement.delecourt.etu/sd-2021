#include <stdio.h>

struct noeud {
  struct noeud* gauche;
  struct noeud* droite;
  int valeur;
};

struct noeud* ajouter_abr(int,struct noeud*);

void afficher_abr(struct noeud*);

int hauteur_abr(struct noeud*);

int nombre_noeuds_abr(struct noeud*);

void clear_abr (struct noeud*);

void affichage_dot(struct noeud*);
