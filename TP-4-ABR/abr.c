#include <stdio.h>
#include <stdlib.h>
#include "abr.h"
#define NIL NULL

static struct noeud* new_feuille (int d)
{
  struct noeud* F;
  F = malloc (sizeof(struct noeud));
  F->gauche = NIL;
  F->droite = NIL;
  F->valeur = d;
  return F;
}

struct noeud* ajouter_abr (int d,struct noeud* A0)
{
  if(A0==NIL)
    return new_feuille(d);
  else
    {
      struct noeud* prec;
      struct noeud* cour;
      cour = A0;
      while(cour != NIL)
	{
	  prec=cour;
	  if(cour->valeur < d)
	    cour=cour->droite;
	  else
	    cour=cour->gauche;
	}
      if (prec->valeur < d)
	prec->droite = new_feuille(d);
      else
	prec->gauche = new_feuille(d);
      return A0;
    }
}

void afficher_abr(struct noeud* A)
{
  if (A != NIL)
    {
      afficher_abr(A->gauche);
      printf("%d\n",A->valeur);
      afficher_abr(A->droite);
    }
}

int hauteur_abr(struct noeud* A)
{
  if (A != NIL)
    {
      int g,d;
      g = hauteur_abr(A->gauche);
      d = hauteur_abr(A->droite);
      if(g < d)
	{
	  return d+1;
	}
      else
	{
	  return g+1;
	}
    }
  else
    {
      return 0;
    }
}

int nombre_noeuds_abr(struct noeud* A)
{
  if (A != NIL)
    {
      return (1 + nombre_noeuds_abr(A->gauche) + nombre_noeuds_abr(A->droite));
    }
  else
    {
      return 0;
    }
}

void clear_abr(struct noeud* A)
{
  if(A != NIL)
    {
      clear_abr(A->gauche);
      clear_abr(A->droite);
      free(A);
    }
}

static void aff_dot(struct noeud* A,FILE* fd)
{
  if (A->gauche != NIL)
    {
      fprintf(fd,"%d -> %d [label=\"gauche\"];\n",A->valeur,A->gauche->valeur);
      aff_dot(A->gauche,fd);
    }
  if (A->droite != NIL)
    {
      fprintf(fd,"%d -> %d [label=\"droite\"];\n",A->valeur,A->droite->valeur);
      aff_dot(A->droite,fd);
    }
}

void affichage_dot(struct noeud* A)
{
  FILE *fd;
  fd = fopen("ABR.dot","w");
  fprintf(fd,"digraph G{");
  if (A != NIL)
    {
      aff_dot(A,fd);
    }
  fprintf(fd,"}");
  fclose(fd);
}
