#include <stdio.h>
#include <stdlib.h>

struct chaine {
  char* caracteres;
  int taille;
};

void constructeur(struct chaine *s)
{
  s->caracteres = (char*)malloc(sizeof(char));
  s->taille = 1;
  //Il faut que la chaine soit terminée par le caractère nul
  s->caracteres[0]='\0';
}

void destructeur(struct chaine *s)
{
  free(s->caracteres);
}

void ajout(struct chaine *s,char c)
{
  s->taille+=1;
  s->caracteres = (char*)realloc(s->caracteres,s->taille*sizeof(char));
  s->caracteres[s->taille-1]='\0';
  s->caracteres[s->taille-2]=c;
}

void imprimer(struct chaine *s)
{
  printf("%s",s->caracteres);
}
