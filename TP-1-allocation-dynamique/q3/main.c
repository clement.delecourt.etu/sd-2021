#include <stdio.h>
#include "chaine.h"

int main()
{
  struct chaine s;
  constructeur(&s);
  ajout(&s,'T');
  ajout(&s,'e');
  ajout(&s,'s');
  ajout(&s,'t');
  imprimer(&s);
  destructeur(&s);
  return 0;
}
