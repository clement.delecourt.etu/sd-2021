struct chaine {
  char* caracteres;
  int taille;
};

void constructeur(struct chaine *s);
void destructeur(struct chaine *s);
void ajout(struct chaine *s,char c);
void imprimer(struct chaine *s);
