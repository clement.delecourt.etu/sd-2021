#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "table_double_hachage.h"
#include "error.h"

/**
 * @file
 * @brief Implantation d'une table de hachage de symboles 
 * avec la technique du double hachage. 
 * @author F. Boulier
 * @date novembre 2010
 */

static long nombre_premier [] =
{ 11, 17, 37, 67, 131, 257, 521, 1031, 2053, 4099, 8209, 16411, 32771, 65537,

  131101, 262147, 524309, 1048583, 2097169, 4194319, 8388617, 16777259,

  33554467, 67108879, 134217757, 268435459, 536870923, 1073741827 };

static struct valeur_double_hachage 
		fonction_double_hachage_par_defaut
			(char* clef, struct table_double_hachage* table)
{   struct valeur_double_hachage hashval;
    long p, i;

    p = table->N;
    hashval.h1 = 0;
    for (i = 0; clef [i] != '\0'; i++)
	hashval.h1 = (hashval.h1 + clef [i] * clef [i]) % p;
    hashval.h2 = 1 + hashval.h1 % (p-1);
    return hashval;
}

static void init_alveole (struct alveole* a)
{
    init_symbole (&a->sym);
    a->etat = alveole_vide;
}

/**
 * @brief Constructeur. Initialise la table \p table avec au moins
 * \p N alvéoles et la fonction de hachage \p h. Le nombre d'alvéoles
 * est arrondi à un nombre premier supérieur ou égal à la valeur passée
 * en paramètre. Si \p h vaut zéro, une fonction de hachage par défaut
 * est choisie.
 * @param[out] table une table de hachage
 * @param[in] N un nombre d'alvéoles souhaité
 * @param[in] h l'adresse d'une fonction de hachage
 */

void init_table_double_hachage 
	(struct table_double_hachage* table, int N, fonction_double_hachage* h)
{   long m, i;
/*
 * La fonction de hachage
 */
    if (h != (fonction_double_hachage*)0)
	table->h = h;
    else
	table->h = &fonction_double_hachage_par_defaut;
/*
 * La taille N est arrondie à un nombre premier
 */
    m = (long)(sizeof (nombre_premier) / sizeof (int));
    i = 0;
    while (i < m && nombre_premier [i] < N)
	i += 1;
    if (i == m)
	table->N = nombre_premier [m-1];
    else
	table->N = nombre_premier [i];
/*
 * Allocation de tab et initialisation des alvéoles
 */
    table->tab = (struct alveole*)malloc 
				(table->N * sizeof (struct alveole));
    if (table->tab == (struct alveole*)0)
	error ("init_table_double_hachage", __FILE__, __LINE__);
    for (i = 0; i < table->N; i++)
	init_alveole (&table->tab [i]);

    table->nb_non_vides = 0;
    table->nb_collisions = 0;
    table->nb_collisions_th = 0.;
    table->lmax_sondages = 0;
}

/**
 * @brief Destructeur. Libère les ressources consommées par \p table.
 * @param[in,out] table une table de hachage
 */

void clear_table_double_hachage (struct table_double_hachage* table)
{   int i;
    for (i = 0; i < table->N; i++)
	clear_symbole (&table->tab [i].sym);
    if (table->tab)
	free (table->tab);
}

/**
 * @brief Retourne le taux de remplissage de la table de hachage,
 * c'est-à-dire le rapport du nombre d'alvéoles non vides divisé
 * par le nombre d'alvéoles alloués.
 * @param[in] table une table de hachage
 */

double taux_double_remplissage_table_double_hachage 
					(struct table_double_hachage* table)
{
    return (double)table->nb_non_vides / (double)table->N;
}

/**
 * @brief Recherche un symbole d'identificateur clef dans la table. 
 * Affecte l'adresse de ce symbole à \p *sym s'il existe, sinon zéro.
 * Retourne le nombre de comparaisons de chaînes de caractères effectuées.
 * @param[out] sym un pointeur sur un symbole
 * @param[in] clef une chaîne de caractères
 * @param[in,out] table une table de hachage
 */

int rechercher_symbole_dans_table_double_hachage 
	(struct symbole** sym, char* clef, struct table_double_hachage* table)
{   struct valeur_double_hachage val;
    double alpha;
    bool found;
    long i;
    int comp, nbcomp, lmax;

    nbcomp = 0;
    lmax = 0;
    val = (*table->h) (clef, table);
    found = false;
    i = val.h1;
    while (table->tab [i].etat != alveole_vide && !found)
    {   if (table->tab [i].etat == alveole_occupe)
	{   nbcomp += 1;
	    comp = strcmp (table->tab [i].sym.ident, clef);
	    if (comp == 0)
		found = true;
	    else
	    {   lmax += 1;
		table->nb_collisions += 1;
#if defined (PRINT_COLLISIONS)
		printf ("collision: %s\t%s\n", table->tab [i].sym.ident, clef);
#endif
	    }
	}
	if (!found)
	    i = (i + val.h2) % table->N;
    }
    if (lmax > table->lmax_sondages)
	table->lmax_sondages = lmax;
    alpha = taux_double_remplissage_table_double_hachage (table);
    table->nb_collisions_th += alpha / (1. - alpha);
    if (found)
	*sym = &table->tab [i].sym;
    else
	*sym = (struct symbole*)0;
    return nbcomp;
}
/**
 * @brief Ajoute le symbole \p sym à la table. 
 * @param[in,out] table une table de hachage
 * @param[in] sym un symbole
 */

void ajouter_symbole_dans_table_double_hachage 
		(struct table_double_hachage* table, struct symbole* sym)
{   struct valeur_double_hachage val;
    bool found;
    long i;
    int comp;

    val = (*table->h) (sym->ident, table);
    found = false;
    i = val.h1;
    while (table->tab [i].etat != alveole_vide && !found)
    {   if (table->tab [i].etat == alveole_occupe)
	{   comp = strcmp (table->tab [i].sym.ident, sym->ident);
	    if (comp == 0)
		found = true;
	}
	if (!found)
	    i = (i + val.h2) % table->N;
    }
    if (found)
	error ("ajouter_symbole_dans_table_double_hachage", __FILE__, __LINE__);
    else
    {   set_symbole (&table->tab [i].sym, sym);
	table->tab [i].etat = alveole_occupe;
	table->nb_non_vides += 1;
    }
}
