#include <stdlib.h>
#include <string.h>
#include "symtable.h"
#include "error.h"

/**
 * @file
 * @brief Implantation des tables des symboles des exécutables
 * @author F. Boulier
 * @date novembre 2010
 */

/**
 * @brief Constructeur. Initialise \p table. Cette fonction devrait
 * être appelée avant toute autre utilisation de \p table.
 * @param[out] table une table des symboles
 */

void init_symtable (struct symtable* table)
{
    table->arbre = NIL;
    init_stats (&table->mesures);
}

/**
 * @brief Destructeur. Libère les ressources consommées par \p table.
 * Cette fonction devrait être appelée après la dernière utilisation
 * de \p table.
 * @param[in,out] table une table des symboles
 */


void clear_symtable (struct symtable* table)
{   
    if (table->arbre != NIL)
    {   clear_ABR (table->arbre);
	free (table->arbre);
    }
    clear_stats (&table->mesures);
}

/**
 * @brief Enregistre le symbole \p sym dans la table des symboles
 * \p table. Le symbole ne doit pas déjà appartenir à
 * la table. Supposons \p sym global. Si un
 * symbole de même nom, indéfini, est déjà présent, ce
 * symbole devient défini. Si un symbole de même nom, défini,
 * est déjà présent, on a affaire à une définition multiple.
 * @param[in,out] table une table des symboles
 * @param[in] sym un symbole
 */

void enregistrer_dans_symtable (struct symtable* table, struct symbole* sym)
{   struct symbole* elt;
    int nbcomp;

/* On incrémente le compteur d'appels à enregistrer et rechercher */
    incrementer_nb_acces_dico_stats (&table->mesures, 1);
    nbcomp = rechercher_symbole_dans_ABR (&elt, sym->ident, table->arbre);
    incrementer_nb_comp_chaines_stats (&table->mesures, nbcomp);
    if (elt)
    {   if (est_defini_symbole (sym))
        {   if (est_indefini_symbole (elt))
                changer_type_symbole (elt, sym->type);
            ajouter_definition_symbole (elt);
        }
    } else
    {
/* Le nb de définitions vaut 1 ou 0 suivant que le symbole est défini ou pas */
        if (est_defini_symbole (sym))
            changer_nbdef_symbole (sym, 1);
        else
            changer_nbdef_symbole (sym, 0);
/* On ne compte pas le nombre de comparaisons parce qu'on aurait pu se
   servir du résultat de la recherche. */
        table->arbre = ajouter_symbole_dans_ABR (table->arbre, sym);
/* On incrémente le compteur de symboles présents dans la table */
        incrementer_nb_symboles_stats (&table->mesures, 1);
    }
    imprimer_stats (&table->mesures);
}

/**
 * @brief Recherche un symbole d'identificateur \p ident 
 * dans la table des symboles de \p table
 * Retourne l'adresse de ce symbole ou le pointeur nul si le symbole
 * est inconnu.
 * @param[in] ident un identificateur de symbole
 * @param[in] table une table des symboles
 */

struct symbole* rechercher_dans_symtable (char* clef, struct symtable* table)
{   int nbcomp;
    struct symbole* elt;

/* On incrémente le compteur d'appels à enregistrer et rechercher */
    incrementer_nb_acces_dico_stats (&table->mesures, 1);
    nbcomp = rechercher_symbole_dans_ABR (&elt, clef, table->arbre);
    incrementer_nb_comp_chaines_stats (&table->mesures, nbcomp);
    imprimer_stats (&table->mesures);
    return elt;
}

/**
 * @brief Constructeur. Initialise \p iter au début de la table des
 * symboles de \p table. Le paramètre \p critere indique la nature des
 * symboles recherchés.
 * Retourne l'adresse du premier symbole
 * satisfaisant ces critères ou le pointeur nul s'il n'y a aucun
 * symbole à énumérer.
 * @param[out] iter un itérateur
 * @param[in] table une table des symboles
 * @param[in] critere la nature des symboles recherchés
 */

struct symbole* first_symbole_symtable 
	(struct iterateur_symtable* iter, 
	 struct symtable* table, enum symbole_recherche critere)
{   struct ABR* ABR;

    iter->table = table;
    init_pile_ABR (&iter->pile);
    for (ABR = table->arbre; ABR != NIL; ABR = ABR->gauche)
	empiler_ABR (&iter->pile, ABR);
    iter->critere = critere;
    return next_symbole_symtable (iter);
}

/**
 * @brief Destructeur. Libère les ressources consommées par \p iter.
 * Cette fonction evrait être appelée après la dernière utilisation
 * de \p iter, ou juste avant le prochain appel à un constructeur.
 * @param[in,out] iter un itérateur
 */

void clear_iterateur_symtable (struct iterateur_symtable* iter)
{
    clear_pile_ABR (&iter->pile);
}

/**
 * @brief Fait avancer l'itérateur \p iter. Retourne l'adresse du
 * symbole suivant ou le pointeur nul s'il n'y a plus de symbole à énumérer.
 * @param[in,out] iter un itérateur
 */

struct symbole* next_symbole_symtable (struct iterateur_symtable* iter)
{   struct ABR* ABR;
    struct ABR* next;

    next = NIL;
    while (! est_vide_pile_ABR (&iter->pile) && next == NIL)
    {	depiler_ABR (&ABR, &iter->pile);
	switch (iter->critere)
	{   case symbole_indefini:
		if (est_indefini_symbole (&ABR->value))
		    next = ABR;
		break;
	    case symbole_duplique:
		if (!est_faible_symbole (&ABR->value) && ABR->value.nbdef >= 2)
		    next = ABR;
		break;
	    case symbole_quelconque:
		next = ABR;
	}
	ABR = ABR->droit;
	while (ABR != NIL)
	{   empiler_ABR (&iter->pile, ABR);
	    ABR = ABR->gauche;
	}
    }
    if (next != NIL)
	return &next->value;
    else
	return (struct symbole*)0;
}

/*
 * @brief Imprime le résultat de l'édition des liens
 * @param[in] table une table des symboles
 */

int synthese_symtable (struct symtable* table)
{
    struct iterateur_symtable iter;
    struct symbole* sym;
    bool ok;

    dot_imprimer_ABR ("linker", table->arbre);

    ok = true;
    sym = first_symbole_symtable (&iter, table, symbole_indefini);
    if (sym != (struct symbole*)0)
    {	printf ("symboles indefinis\n");
	do
	{   printf ("\t%s\n", sym->ident);
	    sym = next_symbole_symtable (&iter);
	} while (sym != (struct symbole*)0);
	ok = false;
    }
    clear_iterateur_symtable (&iter);
    sym = rechercher_dans_symtable ("main", table);
    if (sym == (struct symbole*)0)
    {	printf ("\t%s\n", "main");
	ok = false;
    }
    sym = first_symbole_symtable (&iter, table, symbole_duplique);
    if (sym != (struct symbole*)0)
    {   printf ("symboles dupliques\n");
        do
        {   printf ("\t%s\n", sym->ident);
            sym = next_symbole_symtable (&iter);
        } while (sym != (struct symbole*)0);
	ok = false;
    }
    clear_iterateur_symtable (&iter);
    if (ok)
	printf ("edition des liens reussie\n");
    return ok ? 0 : 1;
}

