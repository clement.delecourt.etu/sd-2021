#if ! defined (STATS_H)
#define STATS_H 1

#include <stdio.h>

/**
 * @file
 * @brief Les types nécessaires à la production de données statistiques
 * permettant de mesurer l'efficacité de la structure de données qui
 * gère la table des symboles.
 * @author F. Boulier
 * @date novembre 2010
 */

/***********************************************************************
 * IMPLANTATION
 ***********************************************************************/

/**
 * @def STATS_FILENAME
 * @brief Le nom du fichier de mesures pour la structure \p struct \p stats.
 */

#define STATS_FILENAME	"linker.stats"

/**
 * @struct stats
 * @brief Le type \p struct \p stats permet de compter des appels de fonctions
 * dans le
 * but de mesurer l'efficacité de la structure \p struct \p symtable.
 * Le champ \p nbget compte les nombres d'appels à
 * \p enregistrer_dans_symtable et \p rechercher_dans_symtable. 
 * Le champ \p nbcomp compte les comparaisons de chaînes de caractères faites
 * par ces deux fonctions. Le champ \p nbsym contient le nombre de
 * symboles présents dans la table des symboles. 
 * Le champ \p f contient le descripteur d'un
 * fichier ouvert en écriture, qui contient les mesures. Le nom du fichier
 * est défini dans \p STATS_FILENAME.
 */

struct stats
{   int nbget;             /**< nombre d'accès au dictionnaire */
    int nbcomp;            /**< compteur de comparaisons */
    int nbsym;		   /**< nombre de symboles dans la structure */
    FILE* f;		   /**< fichier pour les statistiques */
};

/***********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 ***********************************************************************/

extern void init_stats (struct stats*);
extern void clear_stats (struct stats*);
extern void commenter_stats (struct stats*, char*);
extern void incrementer_nb_acces_dico_stats (struct stats*, int);
extern void incrementer_nb_comp_chaines_stats (struct stats*, int);
extern void incrementer_nb_symboles_stats (struct stats*, int);
extern void imprimer_stats (struct stats*);

#endif
