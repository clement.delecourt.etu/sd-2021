#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "liste_double.h"
#include "hachage_simple.h"
#define N 10
#define NIL NULL

void init_table (struct table* T, fonction_hachage* h)
{
  int i;
  for (i=0;i<N;i++)
    {
      init_liste_double(&T->tab[i].L);
    }
  T->hash = h;
}

void clear_table (struct table* T)
{
  int i;
  for(i=0;i<N;i++)
    {
      clear_liste_double(&T->tab[i].L);
    }
}

void enregistrer_table (struct table* T, double d)
{
  ajouter_en_tete_liste_double(&T->tab[T->hash(d)].L,d);
}

bool rechercher_table (struct table* T, double d)
{
  int position;
  position = T->hash(d);
  struct maillon_double* M;
  M = T->tab[position].L.tete;
  while(M != (struct maillon_double*)0)
    {
      if (M->value == d)
	{
	 return 1;
	}
      M = M->next;
    }
  return 0;
}

void imprimer_table (struct table* T)
{
  int i,j;
  struct liste_double L;
  struct maillon_double* M;
  for(i=0;i<N;i++)
    {
      L = T->tab[i].L;
      M = L.tete;
      printf("%d | {",i);
      for(j=0;j < L.nbelem;j++)
	{
	  printf(" %lf ,",M->value);
	  M = M->next;
	}
      printf("}\n");
    }
}
