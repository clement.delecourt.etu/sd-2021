#include <stdio.h>
#include "hachage_simple.h"

int hachage_basique (double d)
{
  return 1;/*(int)d % N;*/
}

int main ()
{
  struct table T;
  double x;
  init_table (&T, &hachage_basique);
  scanf ("%lf", &x);
  while (x != -1)
    {
      enregistrer_table (&T, x);
      imprimer_table (&T);
      scanf ("%lf", &x);
    }
  double rch;
  puts("Entrez un élément à rechercher dans la table\n");
  scanf("%lf",&rch);
  if (rechercher_table(&T,rch))
    {
      printf("Le nombre %lf est bien rangé dans la table\n",rch);
    }
  else
    {
      printf("Le nombre %lf n'est pas rangé dans la table\n",rch);
    }
  clear_table (&T);
  return 0;
}
