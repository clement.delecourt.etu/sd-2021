#include <stdio.h>
#include <stdbool.h>

struct point {
    double x;       /* abscisse */
    double y;       /* ordonnée */
    char ident;     /* identificateur */
};

void init_point(struct point* A, double x, double y, char id)
{
  A->x = x;
  A->y = y;
  A->ident = id;
}

int compare_points (const void* p1, const void* p2)
{
  struct point* A = (struct point*)p1;
  struct point* B = (struct point*)p2;
  if (((A->x)*(B->y)) < ((A->y)*(B->x)))
    {
      return -1;
    }
  else if (((A->x)*(B->y)) == ((A->y)*(B->x)))
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

bool tourne_a_gauche (struct point* A, struct point* B, struct point* C)
{
  struct point AB,AC;
  init_point(&AB,(B->x)-(A->x),(B->y)-(A->y),'B');
  init_point(&AC,(C->x)-(A->x),(C->y)-(A->y),'C');
  return (compare_points(&AB,&AC)==1);
}

